<?php

header('Content-type: application/json; charset=utf-8');

class Event extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('event_model');
    }

    function addEvent() {
        MY_Controller::varifyMethod("POST");

        $getData = MY_Controller::getPostData();

        extract($getData);

        MY_Controller::requiredValidation([
            'iUserID' => $iUserID,
            'vEventTitle' => $vEventTitle,
            'dPrice' => $dPrice,
            'vLocation' => $vLocation,
            'iHostCompany' => $iHostCompany,
            'dEventDate' => $dEventDate,
            'tStartTime' => $tStartTime,
            'tEndTime' => $tEndTime,
            'vLink' => $vLink,
            'tDescription' => $tDescription,
            'tTermsConditions' => $tTermsConditions
        ]);
        if (isset($dLatitude)) {
            $dLatitude = $dLatitude;
        } else {
            $dLatitude = '';
        }
        if (isset($dLongitude)) {
            $dLongitude = $dLongitude;
        } else {
            $dLongitude = '';
        }
        if (isset($tFriendsOnly)) {
            $tFriendsOnly = $tFriendsOnly;
        } else {
            $tFriendsOnly = '';
        }
        if (isset($tShareWith)) {
            $tShareWith = $tShareWith;
        } else {
            $tShareWith = '';
        }

        MY_Controller::checkUserAuthentication($iUserID);
        
        $iEventID = $this->event_model->addEvent($iUserID, $vEventTitle, $dPrice, $vLocation, $dLatitude, $dLongitude, $iHostCompany, $tFriendsOnly, $tShareWith, $dEventDate, $tStartTime, $tEndTime, $vLink, $tDescription, $tTermsConditions);
        
        if (isset($iEventID) && $iEventID > 0 && !empty($getData['images']) && count($getData['images']) > 0) {
            foreach ($getData['images'] as $img) {
                $img = str_replace('data:image/jpeg;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $data = base64_decode($img);
                $vPhoto = uniqid() . '.jpeg';
                $file = IMAGES_EVENT_PATH . $vPhoto;
                file_put_contents($file, $data);
                chmod($file, 0777);
                
                $this->event_model->addEventImage($iEventID, $vPhoto);
            }
        }
        return MY_Controller::responseMessage(1, "Your event was successfully posted.", "True");
    }

    function getEvents() {
        MY_Controller::varifyMethod("POST");

        $getData = MY_Controller::getPostData();

        extract($getData);
        
        MY_Controller::requiredValidation([
            'iUserID' => $iUserID
        ]);

        MY_Controller::checkUserAuthentication($iUserID);
        
        $data['data'] = $this->event_model->getEvents();
        
        return MY_Controller::successResponse($data, 1, 'Events fetch successfully.', 'True');
    }

}
