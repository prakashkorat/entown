<?php
header('Content-type: application/json; charset=utf-8');
class Event_comments extends MY_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('event_comments_model');
    }

    function addComment() {
        MY_Controller::varifyMethod("POST");

        $getData = MY_Controller::getPostData();

        extract($getData);

        MY_Controller::requiredValidation([
            'iEventID' => $iEventID,
            'iUserID' => $iUserID,
            'tComment' => $tComment
        ]);

        MY_Controller::checkUserAuthentication($iUserID);

        $result = $this->event_comments_model->addComment($iEventID, $iUserID, $tComment);

        return MY_Controller::responseMessage(1, "Your comment was successfully posted.", "True");
    }

    function getComment() {
        MY_Controller::varifyMethod("POST");

        $getData = MY_Controller::getPostData();

        extract($getData);

        MY_Controller::requiredValidation([
            'iUserID' => $iUserID,
            'iEventID' => $iEventID
        ]);

        MY_Controller::checkUserAuthentication($iUserID);

        $results = $this->event_comments_model->getComment($iUserID, $iEventID);

        $res = array();

        foreach ($results as $result) {

            $result->vProfilePicThumb = !empty($result->vProfilePic) ? IMAGE_THUMB_URL . $result->vProfilePic : "";
            $result->vProfilePic = !empty($result->vProfilePic) ? IMAGE_URL . $result->vProfilePic : "";

            $res[] = $result;
        }

        $data['data'] = $res;

        return MY_Controller::successResponse($data, 1, 'Comments fetch successfully.', 'True');
    }

}
