<?php
class Main_controller extends CI_Controller
{ 
    function __construct()
    {
        parent::__construct();         
    }
    
    static function varifyMethod($method_type)
    {
        try {
            if ($_SERVER['REQUEST_METHOD'] != $method_type){
                throw new Exception(Main_controller::responseMessage(0, "It seems wrong method type is being used. Please verify method type (GET/POST).", "False"));
                   
            }
        }catch (Exception $e){ exit;}   
    }
    
    static function generateToken() 
    {
        $token = openssl_random_pseudo_bytes(16);
        return $token = bin2hex($token); 
    }
     
    static function requiredValidation($elements_array) 
    {
        foreach ($elements_array as $key => $value) {
            try {
                if (empty($value))
                {
                    throw new Exception(Main_Controller::responseMessage(0, 'Please enter ' . $key . '.', "False"));
                }
            }catch (Exception $e){ exit;}   
        }
    }
    
    static function successResponse($array, $response, $message, $result) 
    {  
        $array['ResponseCode'] = $response; 
        $array['ResponseMsg'] = $message;
        $array['Result'] = $result;
        $array['ServerTime'] = date('T');

        echo json_encode($array);
    }
    
    static function responseMessage($res, $responseMessage, $result)
    {
        echo json_encode(array("ResponseCode"=>$res,"ResponseMsg"=> "$responseMessage","Result"=>"$result","ServerTime"=>date('T')));
    }
    
    static function errorResponse()
    {
        return Main_Controller::responseMessage(0,"It seems web-server is too much busy, please try again.","False");
    }
    
    static function generateRandomCode() 
    {
        $alphabet = '1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return strtoupper(implode($pass));
    }
    
    static function sendMail($email, $subject, $message) 
    {
        $CI =& get_instance();
        $CI->email->set_newline("\r\n");
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $CI->email->initialize($config);
        $CI->email->from(ADMIN_EMAIL);
        $CI->email->to($email);
        $CI->email->subject($subject);
        $CI->email->message($message);
        return $CI->email->send(); 
    }
    
    static function checkUserAuthentication() 
    {
        $user_id = empty($_REQUEST["user_id"]) ? "" : $_REQUEST["user_id"];
        $key = empty($_SERVER["HTTP_KEY"]) ? "" : $_SERVER["HTTP_KEY"];
        $token = empty($_SERVER["HTTP_TOKEN"]) ? "" : $_SERVER["HTTP_TOKEN"];
        Main_Controller::requiredValidation([
            'user_id' => $user_id, 
            'key' => $key, 
            'token' => $token
        ]); 
         
        $user_model = &get_instance()->user_model;
        $result = $user_model->user_model->checkUserAuthentication($user_id, $key, $token);
        
        if($result == 2)
        {
            try{
                throw new Exception(Main_Controller::ResponseMessage(0, 'Something went wrong while generating key, please try again.', "False"));
            }catch (Exception $e){ exit;} 
        }
        else if($result == 3)
        {
             try{
                    throw new Exception(Main_Controller::ResponseMessage(0, 'You are not authorized to access associated web-services; it seems there is mismatch in key-token pair.', "False"));
                }catch (Exception $e){ exit;}   
        }
           
        //$this->load->model('user_model');
//        $user_model = &get_instance()->user_model;
//        $check_key = $user_model->user_model->checkKey($key);
//       
//        if ($check_key->cnt == 0) {
//            try{
//                throw new Exception(Main_Controller::ResponseMessage(0, 'Something went wrong while generating key, please try again.', "False"));
//            }catch (Exception $e){ exit;} 
//        } else { 
//            $check_key_token = $user_model->checkKeyToken($key, $token, $user_id);
//            
//            if ($check_key_token->cnt == 0) {
//                try{
//                    throw new Exception(Main_Controller::ResponseMessage(0, 'You are not authorized to access associated web-services; it seems there is mismatch in key-token pair.', "False"));
//                }catch (Exception $e){ exit;}   
//            }
//        }
    } 
    
    static function uploadImage($image_param_name)
    { 
        $new_image_name = 'image_'. uniqid().'_'.date('YmdHis');
        $config = array(
         'upload_path' => 'images/',
         'file_name' => $new_image_name,
         'allowed_types' => "gif|jpg|png|jpeg",
         'overwrite' => TRUE
         //'max_size' => "1024000"  /* 'max_height' => "768", 'max_width' => "1024" */ // Can be set to particular file size , here it is 1 MB(1024 Kb)
        );
        $CI = &get_instance();
        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);
        
        if($CI->upload->do_upload($image_param_name)){
            $fileData = $CI->upload->data();
            return $file_name = $fileData['file_name'];
        }
    }
    
    static function resizeImage($file_name)
    { 
        $CI =&get_instance();
        $CI->load->library('image_lib');
        $config1['image_library'] = 'gd2';
        $config1['source_image'] = 'images/'.$file_name;
        $config1['create_thumb'] = FALSE;
        $config1['maintain_ratio'] = TRUE;
        $config1['new_image'] = 'images/thumb/'.$file_name;
        $config1['width'] = 100;
        $config1['height'] = 100;

        $CI->image_lib->clear();
        $CI->image_lib->initialize($config1);
        $CI->image_lib->resize();
    }
    
    static function SendSMSUsingTwilio($to_phone_no, $message)
    { 
        $url = "https://api.twilio.com/2010-04-01/Accounts/".TWILIO_ID."/SMS/Messages.json";
        $id = TWILIO_ID;
        $token = TWILIO_TOKEN;
        
        $data = array (
                'From' => TWILIO_FROM_NO,
                'To' => $to_phone_no,       // twilio trial verified number
                'Body' => $message,
            );
        $post = http_build_query($data);
        $x = curl_init($url);
        curl_setopt($x, CURLOPT_POST, true);
        curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
        curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($x, CURLOPT_POSTFIELDS, $post);
        $y = curl_exec($x);
        //print_r($y); exit; 
        curl_close($x);
           
        return;
    }
    
    static function sendPushIos($device_udid_array,$message)
    { 
        $tHost = 'gateway.sandbox.push.apple.com';
        //$tHost = 'gateway.push.apple.com:2195';
        $tPort = 2195;
        $tCert =  IOS_PEM_FILE_PATH;
        $tPassphrase = IOS_PASSPHRASE; 
        $tAlert = $message;
        $tBadge = 0;
        $tSound = 'default';
        $tPayload = 'Success';
        $tBody['aps'] = array ('alert' => $tAlert,'badge' => $tBadge,'sound' => $tSound);
        $tBody ['payload'] = $tPayload;
        $tBody = json_encode ($tBody);
        $tContext = stream_context_create ();
        stream_context_set_option ($tContext, 'ssl', 'local_cert', $tCert);
        stream_context_set_option ($tContext, 'ssl', 'passphrase', $tPassphrase);
        $tSocket = stream_socket_client ('ssl://'.$tHost.':'.$tPort, $error, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $tContext);
        if (!$tSocket)
        {
                exit ("APNS Connection Failed: $error $errstr" . PHP_EOL);
        }
        else
        {
            for($x=0;$x<count($device_udid_array);$x++)
            { 
                $tMsg = chr (0) . chr (0) . chr (32) . pack ('H*', $device_udid_array[$x]) . pack ('n', strlen ($tBody)) . $tBody;
                $tResult = fwrite ($tSocket, $tMsg, strlen ($tMsg));
            } 
        }

        fclose ($tSocket);
        return $tResult;
    }
	
    static function sendPushAndroid($device_udid_array, $message)
    { 
        $url = 'https://fcm.googleapis.com/fcm/send';
 
        $fields = array(
            'registration_ids' => $device_udid_array,
            'data' => array(
               "message" => $message
            ),
            'notification' => array(
              "title" => 'Contactuall',
              "text" => $message
            ),
       );

        $headers = array(
           'Authorization: key=' . ANDROID_AUTHORIZATION_KEY,
           'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch); 
        if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
        }
        //print_r($result); exit; 
        curl_close($ch);
    }
    
    static function responseDate($date)
    {
        return date("M d, Y", strtotime($date));
    }
}