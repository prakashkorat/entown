<?php

header('Content-type: application/json; charset=utf-8');

class User extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('user_model');
    }

    function userSignup() {
        MY_Controller::varifyMethod("POST");

        $getData = MY_Controller::getPostData();

        extract($getData);

        $key = empty($_SERVER["HTTP_KEY"]) ? "" : $_SERVER["HTTP_KEY"];

        MY_Controller::requiredValidation([
            'vEmail' => $vEmail,
            'vPassword' => $vPassword,
            'key' => $key,
            'vDeviceUDID' => $vDeviceUDID,
            'vDeviceType' => $vDeviceType,
            'vDeviceID' => $vDeviceID
        ]);

        $generateToken = MY_Controller::generateToken();

        $result = $this->user_model->userSignup($key, $vEmail, $vPassword, $vDeviceUDID, $vDeviceType, $vDeviceID, $generateToken);

        if ($result == 2)
            return MY_Controller::responseMessage(0, "Something went wrong while generating key, please try again.", "False");
        else if ($result == 3)
            return MY_Controller::responseMessage(0, "This email has already been registered. Sign into your existing account, or register with a different email", "False");
        else {
            $res_data['iUserID'] = $result['iUserID'];
            $res_data['vEmail'] = $result['vEmail'];
            $res_data['token'] = $generateToken;
            $data['data'] = $res_data;

            MY_Controller::successResponse($data, 1, 'Registration has been completed successfully.', 'True');
        }
    }
    
    function userLogin(){
        MY_Controller::varifyMethod("POST");

        $getData = MY_Controller::getPostData();

        extract($getData);

        $key = empty($_SERVER["HTTP_KEY"]) ? "" : $_SERVER["HTTP_KEY"];

        MY_Controller::requiredValidation([
            'vEmail' => $vEmail,
            'vPassword' => $vPassword,
            'key' => $key,
        ]);

        $generateToken = MY_Controller::generateToken();

        $result = $this->user_model->userLogin($key, $vEmail, $vPassword, $generateToken);

        if (!is_array($result) && $result == 2)
            return MY_Controller::responseMessage(0, "Something went wrong while generating key, please try again.", "False");
        else if (!is_array($result) && $result == 3)
            return MY_Controller::responseMessage(0, "Wrong email or password", "False");
        else {
            $res_data['iUserID'] = $result['iUserID'];
            $res_data['vEmail'] = $result['vEmail'];
            $res_data['token'] = $generateToken;
            $data['data'] = $res_data;

            MY_Controller::successResponse($data, 1, 'Login successfully.', 'True');
        }
    }
}
