<?php
class Event_comments_model extends CI_Model 
{
    function __construct() 
    { 
        parent::__construct();
    }
    
    function addComment($iEventID, $iUserID, $tComment)
    { 
    	$data = array(
            'iEventID'     	=> $iEventID,
            'iUserID'    	=> $iUserID,
            'tComment'     	=> $tComment,
            'dCreatedDate'  => date('Y-m-d H:i:s'),
            'tModifyDate'   => date('Y-m-d H:i:s')
        );

        return $this->db->insert('event_comments', $data);
    }

    function getComment($iUserID, $iEventID)
    {
    	$data = array(
            'ec.iUserID'    => $iUserID,
            'ec.iEventID'    => $iEventID
        );
        
    	$this->db->select('u.vFirstName, u.vSecondName, u.vProfilePic, ec.*');
        $this->db->from('event_comments ec');
 		$this->db->join('users u', 'ec.iUserID = u.iUserID');
        $this->db->where($data);
        $query = $this->db->get();
		return $query->result();
    }
      
} 

?>
