<?php

class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function checkUserAuthentication($iUserID, $token) {
        $data = array(
            'iUserID' => $iUserID,
            'vToken' => $token
        );

        $query = $this->db->get_where('users', $data);
        return $query->num_rows();
    }

    function userSignup($key, $vEmail, $vPassword, $vDeviceUDID, $vDeviceType, $vDeviceID, $generateToken) {
        $data = array(
            'vEmail' => $vEmail
        );
        $query = $this->db->get_where('users', $data);

        if (AUTH_KEY != $key) {
            return 2;
        }

        if ($query->num_rows() > 0) {
            return 3;
        } else {

            $data = array(
                'vEmail' => $vEmail,
                'vPassword' => md5($vPassword),
                'vDeviceUDID' => $vDeviceUDID,
                'vDeviceType' => $vDeviceType,
                'vDeviceID' => $vDeviceID,
                'vToken' => $generateToken,
                'dCreatedDate' => date('Y-m-d H:i:s'),
                'tModifyDate' => date('Y-m-d H:i:s')
            );
            $this->db->insert('users', $data);
            $returnArray = array();
            $returnArray['iUserID'] = $this->db->insert_id();
            $returnArray['vEmail'] = $vEmail;
            return $returnArray;
        }
    }
    function userLogin($key, $vEmail, $vPassword, $generateToken){
        if (AUTH_KEY != $key) {
            return 2;
        }
        $data = array(
            'vEmail' => $vEmail,
            'vPassword' => md5($vPassword)
        );
        $query = $this->db->get_where('users', $data);
        if ($query->num_rows() > 0) {
            $data = array(
                'vToken' => $generateToken
            );
            $this->db->where('vEmail',$vEmail);
            $this->db->update('users',$data);

            return $query->row_array();
        }else{
            return 3;
        }
    }
}

?>
