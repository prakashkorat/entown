<?php
class Comman_model extends CI_Model 
{
    private static $db;
    function __construct()
    {
        parent::__construct(); 
        self::$db = &get_instance()->db;
    }
    
    static function queryData($query)
    {   
         try {
            $result = self::$db->query($query);
            if ( ! $result){
               throw new Exception(comman_controller::errorResponse());
            } else {
                return $result;
            }
        }catch (Exception $e){exit;}  
    }
    
    static function selectData($table,$where=array(),$cell='*')
    { 
        try {
            self::$db->select($cell);
            $result = self::$db->get_where($table,$where);
            if ( ! $result){
               throw new Exception(comman_controller::errorResponse());
            } else {
                return $result->result_array();
            }
        }catch (Exception $e){exit;}      
    }
    
    static function selectSingleRow($table,$where=array(),$cell='*')
    {
        try{
            self::$db->select($cell);
            $result = self::$db->get_where($table,$where);
            if($result){
                    return $result->row();	
            } else {
                    throw new Exception(comman_controller::errorResponse());
            }
        }catch (Exception $e){exit;}
    }
    
    static function insertData($table,$data)
    {
        try{
            if(self::$db->insert($table, $data)){
                    return self::$db->insert_id();
            } else {
                    throw new Exception(comman_controller::errorResponse());
            }
        }catch (Exception $e){exit;}
    }
    
    static function updateData($table,$data,$where=array())
    {
        try{
            $result = self::$db->update($table, $data, $where);
            if($result){
                    return 1;	
            } else {
                    throw new Exception(comman_controller::errorResponse());
            }
        }catch (Exception $e){exit;}
    }
     
    static function deleteData($table,$where=array())
    {
        try{
            if(self::$db->delete($table,$where)){
                    return 1;	
            } else {
                    throw new Exception(comman_controller::errorResponse());
            }
        }catch (Exception $e){exit;}
    }
    
    static function getDataSP($table_name="",$colunm_name="*",$where_condition)
    { 
        $table_name = $table_name; 
        $colunm_name = $colunm_name; 
       
        $where_condition = self::$db->escape_str($where_condition);  
        $query = "CALL GET_DATA('$table_name','$colunm_name','$where_condition')";  
        //echo $query; exit; 
        try {
            $result = self::$db->query($query);
            if ( ! $result){
               throw new Exception(comman_controller::errorResponse());
            } else {
                return $result;
            }
        }catch (Exception $e){exit;}  
    }
}
	