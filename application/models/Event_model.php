<?php

class Event_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function addEvent($iUserID, $vEventTitle, $dPrice, $vLocation, $dLatitude, $dLongitude, $iHostCompany, $tFriendsOnly, $tShareWith, $dEventDate, $tStartTime, $tEndTime, $vLink, $tDescription, $tTermsConditions) {
        $data = array(
            'iUserID' => $iUserID,
            'vEventTitle' => $vEventTitle,
            'dPrice' => $dPrice,
            'vLocation' => $vLocation,
            'dLatitude' => $dLatitude,
            'dLongitude' => $dLongitude,
            'iHostCompany' => $iHostCompany,
            'dEventDate' => $dEventDate,
            'tStartTime' => $tStartTime,
            'tEndTime' => $tEndTime,
            'vLink' => $vLink,
            'tFriendsOnly' => $tFriendsOnly,
            'tShareWith' => $tShareWith,
            'tDescription' => $tDescription,
            'tTermsConditions' => $tTermsConditions,
            'dCreatedDate' => date('Y-m-d H:i:s'),
            'tModifyDate' => date('Y-m-d H:i:s')
        );
        $this->db->insert('events', $data);
        return $this->db->insert_id();
    }

    function addEventImage($iEventID, $vPhoto) {
        $data = array(
            'iEventID' => $iEventID,
            'vPhoto' => $vPhoto
        );
        $this->db->insert('event_photos', $data);
        return $this->db->insert_id();
    }

    function getEvents() {
        $this->db->select('*');
        $this->db->from('events');
        $query = $this->db->get();
        $temp = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $val) {
                $data = array(
                    'iEventID' => $val['iEventID']
                );
                $query1 = $this->db->get_where('event_photos', $data);
                if ($query1->num_rows() > 0) {
                    $val['images'] = $query1->result_array();
                }
                $temp[] = $val;
            }
        }
        return $temp;
    }

}

?>
